To start the project
1. Need to install docker and docker-compose
https://docs.docker.com/compose/install/

2. Build and run the project in the first time <br>
**sudo  docker-compose up -d --build**

    next times  <br>
    **sudo  docker-compose up -d**
    
3. Clean cache <br>
**sudo docker-compose exec app php artisan optimize**

4. Download PHP packages <br>
**sudo docker-compose exec app composer update**

or clean cahce
**sudo docker-compose exec app composer dump-autoload -o**

5. Run migrations <br>
**sudo docker-compose exec app php artisan migrate**

Project url 
http://0.0.0.0:8080/


PS. Please, copy file .env.example and rename to .env
